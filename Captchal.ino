// Touch screen library with X Y and Z (pressure) readings as well
// as oversampling to avoid 'bouncing'
// This demo code returns raw readings, public domain

#include <stdint.h>
#include "TouchScreen.h"
// These are the pins for the shield!
#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 3   // can be a digital pin

#define MINPRESSURE 10
#define MAXPRESSURE 1000
#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

// For the Adafruit shield, these are the default.
#define TFT_DC 9
#define TFT_CS 10

// Use hardware SPI (on Uno, #13, #12, #11) and the above for CS/DC
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

void setup(void) {
  Serial.begin(9600);
  tft.begin();
  pinMode(6, INPUT);
  pinMode(5, INPUT);
  animateTitleScreen();
}
int teamWithFirstCap = 0;
int maxTime = 3;
int timeGame = 3;
int pageNo = 0;
int teamSide = 0;
int teamSeconds = 0;
int prevTeam = teamSide;
int increment = 1;
int redTime = 0;
int greenTime = 0;
float prevMil = millis();
float circumOfCircle = 0;
void drawCircleFunc(int x, int y, int radius, int color, float circ){
  for (int i=65; i<circ+65; i++)  {
    double radians = i * PI / 180;
    double px = x + radius * cos(radians);
    double py = y + radius * sin(radians);
    tft.drawPixel(px, py, color);
  }
}
void loop(void) {
  if (pageNo == 2) {
    if (digitalRead(6) == HIGH && digitalRead(5) == HIGH) {
      if (teamSide != 0) {
        teamSide = 0;
        updateGame();
      }
      teamSide = 0;
    }
    else if (digitalRead(6) == HIGH) {
      teamSide = 2;
      if (teamWithFirstCap == 0) {
        teamWithFirstCap = 2;
      }
    }
    else if (digitalRead(5) == HIGH) {
      teamSide = 1;
      if (teamWithFirstCap == 0) {
        teamWithFirstCap = 1;
      }
    }
  }
  if (digitalRead(6) == HIGH && digitalRead(5) == HIGH && pageNo == 3) {
    timeGame = maxTime;
    animateTitleScreen();
  }
  // a point object holds x y and z coordinates
  TSPoint p = ts.getPoint();
  // we have some minimum pressure we consider 'valid'
  // pressure of 0 means no pressing!
  if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
    Serial.print("X = "); Serial.print(p.x);
    Serial.print("\tY = "); Serial.print(p.y);
    Serial.print("\tPressure = "); Serial.println(p.z);
    if (p.x >= 260 && p.x <= 420 && p.y >= 130 && p.y <= 450 && pageNo == 0) {
      changeTime();
    }
    else if (p.x >= 530 && p.x <= 730 && p.y >= 550 && p.y <= 750 && pageNo == 1 && timeGame < 9) {
      timeGame++;
      maxTime++;
      tft.fillRect(30, 100, 30, 50, ILI9341_BLACK);
      tft.setCursor(30, 100);
      tft.setTextColor(ILI9341_BLUE);
      tft.setTextSize(6);
      tft.println(timeGame);
      delay(250);
    }
    else if (p.x >= 320 && p.x <= 520 && p.y >= 550 && p.y <= 750 && pageNo == 1 && timeGame > 1) {
      timeGame--;
      maxTime--;
      tft.fillRect(30, 100, 30, 50, ILI9341_BLACK);
      tft.setCursor(30, 100);
      tft.setTextColor(ILI9341_BLUE);
      tft.setTextSize(6);
      tft.println(timeGame);
      delay(250);
    }
    else if (p.x >= 160 && p.x <= 330 && p.y >= 140 && p.y <= 440 && pageNo == 1) {
      animateTitleScreen();
    }
    else if (p.x >= 230 && p.x <= 490 && p.y >= 530 && p.y <= 870 && pageNo == 0) {
      beginGame();
    }
  }
  if (pageNo == 2 && teamSide != 0 && millis() - 1000 >= prevMil) {
    prevMil = millis();
    if (timeGame == maxTime) {
      increment = -1;
    }
    if (increment == -1) {
      if (teamSeconds != 0) {
        teamSeconds += increment;
      }
      else {
        teamSeconds = 59;
        timeGame += increment;
      }
    }
    else {
      if (teamSeconds != 59) {
        teamSeconds += increment;
      }
      else {
        teamSeconds = 0;
        timeGame += increment;
      }
    }
    updateGame();
    if (teamSide != prevTeam) {
      increment *= -1;
    }
    if (timeGame == maxTime) {
      increment = -1;
    }
    prevTeam = teamSide;
    if (teamSide == 1) {
      redTime++;
    }
    if (teamSide == 2) {
      greenTime++;
    }
  }
}
unsigned long animateTitleScreen() {
  pageNo = 0;
  tft.fillScreen(ILI9341_BLACK);
  tft.setRotation(1);
  testCircles(6,ILI9341_BLUE);
  tft.setCursor(60, 20);
  tft.setTextColor(ILI9341_RED);
  tft.setTextSize(4);
  tft.println("CAPT");
  tft.setTextColor(ILI9341_GREEN);
  tft.setCursor(160, 20);
  tft.println("CHAL");
  tft.fillRect(15, 150, 125, 50, ILI9341_RED);
  tft.fillRect(180, 150, 125, 50, ILI9341_GREEN);
  tft.setCursor(20, 160);
  tft.setTextColor(ILI9341_GREEN);
  tft.setTextSize(2);
  tft.println("Change");
  tft.println("       Time");
  tft.setCursor(185, 160);
  tft.setTextColor(ILI9341_RED);
  tft.println("Start");
  tft.setCursor(170, 175);
  tft.println("       Game");
}
unsigned long testLines(uint16_t color) {
  int           x1, y1, x2, y2,
                w = tft.width(),
                h = tft.height();
  
  x1 = y1 = 0;
  y2    = h - 1;
  for(x2=0; x2<w; x2+=6) tft.drawLine(x1, y1, x2, y2, color);
  x2    = w - 1;
  for(y2=0; y2<h; y2+=6) tft.drawLine(x1, y1, x2, y2, color);
}
unsigned long testFilledTriangles() {
  int           i, cx = tft.width()  / 2 - 1,
                   cy = tft.height() / 2 - 1;

  for(i=min(cx,cy); i>10; i-=5) {
    tft.fillTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(0, i*10, i*10));
    tft.drawTriangle(cx, cy - i, cx - i, cy + i, cx + i, cy + i,
      tft.color565(i*10, i*10, 0));
  }
}
unsigned long changeTime() {
  tft.fillScreen(ILI9341_BLACK);
  tft.fillRect(0,0,400,400,ILI9341_BLACK);
  testFilledTriangles();
  pageNo = 1;
  tft.setCursor(35, 20);
  tft.setTextColor(ILI9341_BLUE);
  tft.setTextSize(3);
  tft.println("CONFIGURE TIME");
  tft.setCursor(30, 100);
  tft.setTextColor(ILI9341_BLUE);
  tft.setTextSize(6);
  tft.println(timeGame);
  tft.setCursor(60, 100);
  tft.print(":00");
  tft.fillCircle(225, 90, 20, ILI9341_RED);
  tft.fillCircle(225, 150, 20, ILI9341_GREEN);
  tft.fillCircle(50, 200, 30, ILI9341_ORANGE);
  tft.fillCircle(100, 200, 30, ILI9341_ORANGE);
  tft.setCursor(215, 77);
  tft.setTextColor(ILI9341_GREEN);
  tft.setTextSize(4);
  tft.println("+");
  tft.setCursor(215, 137);
  tft.setTextColor(ILI9341_RED);
  tft.setTextSize(4);
  tft.println("-");
  tft.setCursor(30, 185);
  tft.setTextColor(ILI9341_GREEN);
  tft.setTextSize(4);
  tft.println("HOME");
}
unsigned long updateGame() {
  drawMiddleGame();
  if (timeGame <= 0 && teamSeconds <= 0) {
    pageNo = 3;
    resultsPage();
  }
}
unsigned long testCircles(uint8_t radius, uint16_t color) {
  int           x, y, r2 = radius * 2,
                w = tft.width()  + radius,
                h = tft.height() + radius;
  for(x=0; x<w; x+=r2) {
    for(y=0; y<h; y+=r2) {
      tft.drawCircle(x, y, radius, color);
    }
  }
}
unsigned long resultsPage() {
  tft.fillScreen(ILI9341_WHITE);
  tft.setCursor(30, 20);
  tft.setTextColor(ILI9341_BLUE);
  tft.setTextSize(6);
  tft.println("RESULTS");
  tft.setCursor(30, 70);
  tft.setTextSize(4);
  tft.println("Winner:");
  tft.setCursor(200, 70);
  if ((teamSide == 1 && increment == -1) || (teamSide == 2 && increment == 1)) {
    tft.setTextColor(ILI9341_RED);
    tft.println("RED");
  }
  else {
    tft.setTextColor(ILI9341_GREEN);
    tft.println("GREEN");
  }
  tft.setCursor(30, 110);
  tft.setTextSize(2);
  tft.setTextColor(ILI9341_BLUE);
  tft.println("First To Capture:");
  tft.setCursor(250, 110);
  if (teamWithFirstCap == 1) {
    tft.setTextColor(ILI9341_RED);
    tft.println("RED");
  }
  else {
    tft.setTextColor(ILI9341_GREEN);
    tft.println("GREEN");
  }
  tft.setCursor(30, 140);
  tft.setTextSize(2);
  tft.setTextColor(ILI9341_BLUE);
  tft.println("Red Ball Time:");
  tft.setCursor(200, 140);
  tft.setTextColor(ILI9341_RED);
  tft.println(redTime);
  tft.setCursor(250, 140);
  tft.println("secs");
  tft.setCursor(30, 170);
  tft.setTextColor(ILI9341_BLUE);
  tft.println("Green Ball Time:");
  tft.setCursor(220, 170);
  tft.setTextColor(ILI9341_GREEN);
  tft.println(greenTime);
  tft.setCursor(260, 170);
  tft.println("secs");
  
}
unsigned long drawMiddleGame() {
  tft.fillCircle(160, 150, 80, ILI9341_BLACK);
  if (teamSide == 0) {
    tft.drawCircle(160, 150, 75, ILI9341_BLUE);
    tft.setTextColor(ILI9341_BLUE);
  }
  else if (teamSide == 1) {
    tft.drawCircle(160, 150, 75, ILI9341_RED);
    tft.setTextColor(ILI9341_RED);
    Serial.println((timeGame + (teamSeconds / 60)));
  }
  else if (teamSide == 2) {
    tft.drawCircle(160, 150, 75, ILI9341_GREEN);
    tft.setTextColor(ILI9341_GREEN);
  }
  tft.setCursor(115, 135);
  tft.setTextSize(4);
  tft.println(timeGame);
  tft.setCursor(135, 135);
  tft.print(":");
  tft.setCursor(160, 135);
  if (teamSeconds > 9) {
    tft.print(teamSeconds);
  }
  else {
    tft.print("0");
    tft.setCursor(185, 135);
    tft.print(teamSeconds);
  }
}
unsigned long beginGame() {
  teamWithFirstCap = 0;
  timeGame = maxTime;
  teamSide = 0;
  teamSeconds = 0;
  prevTeam = teamSide;
  increment = 1;
  redTime = 0;
  greenTime = 0;
  tft.fillRect(0,0,400,400,ILI9341_BLACK);
  tft.fillScreen(ILI9341_BLACK);
  testLines(ILI9341_YELLOW);
  pageNo = 2;
  tft.setCursor(50, 20);
  tft.setTextColor(ILI9341_BLUE);
  tft.setTextSize(3);
  tft.println("GAME SESSION");
  drawMiddleGame();
}

